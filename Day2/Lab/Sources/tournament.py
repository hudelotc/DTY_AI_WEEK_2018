import os
import os.path
import pickle
import logging
import numpy
import random
import sys

import utils
from ui_game import UIGame


class Tournament(object):
    CHECKPOINT_NAME = 'checkpoint.pickle'
    FINAL_NAME = 'final.pickle'

    def __init__(self, folder):
        iaKlasses = utils.loadClasses(os.path.join(folder, "*"), "Player")
        self.ias = [ia() for ia in iaKlasses]
        self.scores = numpy.zeros((len(self.ias), len(self.ias)))
        self.getPairs()
        self.dbg = ""
        self.current = 0

    def run(self):
        start = self.current
        for i, j in self.pairs[start:]:
            player1 = self.ias[i]
            self.initAIPlayer(player1)
            player2 = self.ias[j]
            self.initAIPlayer(player2)

            game = UIGame(player1, player2, self.dbg)
            game.run()
            if game.winner:
                if game.winner.color == player1.color:
                    self.scores[i, j] += 1
                else:
                    self.scores[j, i] += 1

            totals = sorted([
                (sum(self.scores[i, :]), self.ias[i].name) for i in range(
                    len(self.ias))], reverse=True)
            self.dbg = "\n".join(
                map(lambda x: '{0}: {1:.0f}'.format(x[1], x[0]), totals))
            self.current += 1

            self.save(final=False)

        self.save(final=True)
        logging.info(self.dbg)

    def save(self, final=False):
        filename = self.FINAL_NAME if final else self.CHECKPOINT_NAME
        with open(filename, 'wb') as fp:
            pickle.dump(self, fp)

        if final and os.path.exists(self.CHECKPOINT_NAME):
            os.remove(self.CHECKPOINT_NAME)

        with open('result.txt', 'w') as fp:
            fp.write(self.dbg)

    def getPairs(self):
        numPlayers = len(self.ias)
        self.pairs = list(filter(
            lambda x: x[0] != x[1],
            [(i, j) for i in range(numPlayers) for j in range(numPlayers)]))
        random.shuffle(self.pairs)

    def initAIPlayer(self, player):
        """In case the player has no name, set we find one based on its name."""
        player.__init__()
        if not getattr(player, 'name', None):
            filename = sys.modules[player.__module__].__file__
            name = os.path.basename(filename)[len('iaplayer'):-3].strip('_')
            setattr(player, 'name', name)
