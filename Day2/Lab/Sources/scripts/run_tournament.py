import argparse
import logging
import os.path
import pickle

from tournament import Tournament


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder', default='players/')
    args = parser.parse_args()

    if os.path.exists(Tournament.CHECKPOINT_NAME):
        with open(Tournament.CHECKPOINT_NAME, 'rb') as fp:
            tournament = pickle.load(fp)
    else:
        tournament = Tournament(args.folder)

    tournament.run()
